// Variaveis globais a serem utilizadas
var janelaTamanho = 2.4;
var portaTamanho = 1.52;
var m2LitroTinta = 5;
var latas = [18, 3.6, 2.5, 0.5];
var somaMetros = 0;
var hasError = false;

jQuery(document).ready(function ($) {
  
  //Inicia o processamento de informacoes no click do botao CALCULAR VALORES
  $("#btn-calc").click(function () {

    //Reinicia os dados da tabela para default
    $("#table").html("");

    //Define como Zero o valor da soma de metros quadrados
    somaMetros = 0;

    //Armazena os dados dos inputs refente a Parede 01
    var altura = $(".altura01").val();
    var largura = $(".largura01").val();
    var porta = $(".pt01").val();
    var janela = $(".jn01").val();

    //Passa os seguintes parametros para a function Validate
    validate("Parede 01", altura, largura, porta, janela);

    //Armazena os dados dos inputs refente a Parede 02
    altura = $(".altura02").val();
    largura = $(".largura02").val();
    porta = $(".pt02").val();
    janela = $(".jn02").val();

    //Passa os seguintes parametros para a function Validate
    validate("Parede 02", altura, largura, porta, janela);

    //Armazena os dados dos inputs refente a Parede 03
    altura = $(".altura03").val();
    largura = $(".largura03").val();
    porta = $(".pt03").val();
    janela = $(".jn03").val();

    //Passa os seguintes parametros para a function Validate
    validate("Parede 03", altura, largura, porta, janela);

    //Armazena os dados dos inputs refente a Parede 04
    altura = $(".altura04").val();
    largura = $(".largura04").val();
    porta = $(".pt04").val();
    janela = $(".jn04").val();

    //Passa os seguintes parametros para a function Validate
    validate("Parede 04", altura, largura, porta, janela);

    //Verifica se tem erros nas validacoes da function
    if (!hasError)
      //Inicia a funcao calculate apos validar as informacoes das quatro paredes
      calculate();
    else
      //Se ouver algum erro ele reseta a variavel para comecar o processo novamente
      hasError = false;
  });

});

//Function para validar as informacoes preenchidas pelo usuario
function validate(parede, altura, largura, porta, janela) {
  if (!hasError) {

    //Realiza o calculo de m² da parede
    var paredeM2 = altura * largura;

    //Realiza o calculo de m² de janela(s)
    var janelaM2 = janelaTamanho * janela;

    //Realiza o calculo de m² de porta(s)
    var portaM2 = portaTamanho * porta;

    //Soma o valor em m² de janela(s) e porta(s)
    var pjM2 = janelaM2 + portaM2;

    //Verifica se existe porta na parede
    if (porta >= 1) {
      //Calcula se a parede é 30cm maior que a porta
      var validaParede = altura - 1.9;

      //Valida se a parede é 30cm maior que a porta
      if (validaParede <= 0.29) {
        //Explode um erro caso a parede não seja maior que 30cm que a porta
        alert("Erro na parede (" + parede + "): A altura da parede precisa ser 30cm maior que a altura da porta.");
        hasError = true;
        return;
      }
    }

    //Verifica se o tamanho em m² de janela(s) e porta(s) excedem o limite de 50% da area da parede
    if (pjM2 > paredeM2 / 2) {
      //Explode um alerta informando ao usuario o limite de 50% da area da parede
      alert("Erro na parede (" + parede + "): A área das janelas somadas com as portas não podem passar de 50% da área da parede");
      hasError = true;
      return;
    }

    //Realiza o calculo para saber qual a área a ser pintado já retirando os valores de janela e porta
    paredeM2 = (paredeM2 - janelaM2 - portaM2);

    //Verifica se o tamanho em m² da parede é entre 1 e 15m²
    if (paredeM2 <= 0 || paredeM2 >= 15) {
      alert("Erro na parede (" + parede + "): A parede não pode ter menos que 1m² e nem mais que 15m²!");
      hasError = true;
      return;
    }

    //Adiciona na tabela de resultado a linha com as informações rorrespondentes a cada parede
    $("#table").append("<tr><td>" + parede + "</td>" + "<td>" + paredeM2 + "m²</td></tr>");

    //Realiza a soma da quantidade em m² de todas as paredes
    somaMetros = somaMetros + paredeM2;
    
    //Verifica se foi realizado os calculos para a Parede 04
    if (parede == "Parede 04")
    //Adiciona a soma total de m² de todas as paredes na tabela
      $("#table").append("<tr><td>Total</td>" + "<td>" + somaMetros + "m²</td></tr>");

  }
}

//Function para calcular a quantidade de litros de tintas a serem utilizados
function calculate() {
  //Realiza o calculo de quantos litros de tinta a pessoa ira utilizar
  var tinta = Math.ceil(somaMetros / m2LitroTinta);
  //Explode um aviso informando a quantidade de litros de tinta
  alert("Você precisará de: " + tinta + "L de tinta.");

}